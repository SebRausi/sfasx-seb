﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour
{
    [SerializeField] private Camera MainCamera;
    [SerializeField] private Character Character;
    [SerializeField] private Canvas Menu;
    [SerializeField] private Canvas Hud;
    [SerializeField] private Transform CharacterStart;

    private RaycastHit[] mRaycastHits;
    private Character mCharacter;
    private EnvironmentController mMap;
    private float cameraMoveSpeed = 20.0f;
    private Vector3 cameraStartPosition;
    private Vector3 cameraCurrentPosition;
    private bool isPlaying;

    private readonly int NumberOfRaycastHits = 1;

    private static GameController instance;
    public static GameController Instance { get { return instance; } }

    void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }

        cameraStartPosition = MainCamera.transform.position;

    }

    void Start()
    {
        isPlaying = false;
        mRaycastHits = new RaycastHit[NumberOfRaycastHits];
        mMap = GetComponentInChildren<EnvironmentController>();
        mCharacter = Instantiate(Character, transform); // create character
        ShowMenu(true);
    }

    private void Update()
    {
        // This needs to be modified in order to determine a start and end tile for the enemy AI / player generated units
        
        // Check to see if the player has clicked a tile and if they have, try to find a path to that 
        // tile. If we find a path then the character will move along it to the clicked tile. 
        if(Input.GetMouseButtonDown(0))
        {
            Ray screenClick = MainCamera.ScreenPointToRay(Input.mousePosition);
            int hits = Physics.RaycastNonAlloc(screenClick, mRaycastHits);
            if( hits > 0)
            {
                EnvironmentTile tile = mRaycastHits[0].transform.GetComponent<EnvironmentTile>();
                if (tile != null)
                {
                    // The route from playerBase to enemyBase and viceverse
                    List<EnvironmentTile> route = mMap.Solve(mCharacter.CurrentPosition, tile);
                    mCharacter.GoTo(route);
                }
            }
        }
        
    }

    private void LateUpdate()
    {
        if (isPlaying) 
        {
            MoveCamera();
        }
    }

    private void MoveCamera()
    {
        
        //WASD to move X and Z .. Check camera specs for strategy game, orientation and perspective - orthographic or isometric
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            MainCamera.transform.localPosition += transform.forward * cameraMoveSpeed * Time.deltaTime; 
        }
        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) 
        {
            MainCamera.transform.localPosition += transform.forward * -cameraMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            MainCamera.transform.localPosition += transform.right * cameraMoveSpeed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            MainCamera.transform.localPosition += transform.right * -cameraMoveSpeed * Time.deltaTime;
        }

        //This little peace of code is written by JelleWho https://github.com/jellewie
        float ScrollWheelChange = Input.GetAxis("Mouse ScrollWheel");           
        if (ScrollWheelChange != 0)
        {                                            
            //If the scrollwheel has changed
            //The radius from current camera
            float R = ScrollWheelChange * 100;                                   
            //Get up and down
            float PosX = MainCamera.transform.eulerAngles.x + 90;              
            //Get left to right
            float PosY = -1 * (MainCamera.transform.eulerAngles.y - 90);       
            //Convert from degrees to radians
            PosX = PosX / 180 * Mathf.PI;                                       
            PosY = PosY / 180 * Mathf.PI;
            //Calculate new coords
            float X = R * Mathf.Sin(PosX) * Mathf.Cos(PosY);                    
            float Z = R * Mathf.Sin(PosX) * Mathf.Sin(PosY);                    
            float Y = R * Mathf.Cos(PosX);
            //Get current camera postition for the offset
            float CamX = MainCamera.transform.position.x;                      
            float CamY = MainCamera.transform.position.y;                      
            float CamZ = MainCamera.transform.position.z;

            cameraCurrentPosition = new Vector3(CamX, CamY, CamZ);
            Vector3 finalCameraPosition = new Vector3(CamX + X, CamY + Y, CamZ + Z);

            Vector3 newCameraPosition = Vector3.Lerp(cameraCurrentPosition, finalCameraPosition, 10.0f);
            //Move the main camera
            MainCamera.transform.position = newCameraPosition;
            
        }
        // End of little piece of code

        // Camera Pan Limits
   
        if (MainCamera.transform.position.y <= 120.0f)
        {
            MainCamera.transform.position = new Vector3(MainCamera.transform.position.x, 120.0f, cameraCurrentPosition.z);
            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
            {
                MainCamera.transform.position = new Vector3(MainCamera.transform.position.x, 120.1f, cameraCurrentPosition.z);
            }
        }
        else if (MainCamera.transform.position.y >= 200.0f)
        {
            MainCamera.transform.position = new Vector3(MainCamera.transform.position.x, 150.0f, MainCamera.transform.position.z);
        }
        if (MainCamera.transform.position.z >= -100.0f)
        {
            MainCamera.transform.position = new Vector3(MainCamera.transform.position.x, MainCamera.transform.position.y, -100.0f);
        }

        if(MainCamera.transform.position.x <= -100.0f)
        {
            MainCamera.transform.position = new Vector3(-100.0f, MainCamera.transform.position.y, MainCamera.transform.position.z);
        }
    }

    public void ShowMenu(bool show)
    {
        if (Menu != null && Hud != null)
        {
            Menu.enabled = show;
            Hud.enabled = !show;

            if( show )
            {
                MainCamera.transform.position = cameraStartPosition;
                mCharacter.transform.position = CharacterStart.position;
                mCharacter.transform.rotation = CharacterStart.rotation;
                // Cleans up the world each time Menu is shown
                // This has been commented as we are now cleaning on generate, if we press play or clean on display it will result in a fatal error
                //mMap.CleanUpWorld(); 
                isPlaying = false;
            }
            else
            {
                mCharacter.transform.position = mMap.Start.Position; // The Start tile of the map, the position assigned from Generate method in Environment
                mCharacter.transform.rotation = Quaternion.identity;
                mCharacter.CurrentPosition = mMap.Start;
                isPlaying = true;
            }
        }
    }

    public void Generate() // called from button
    {
        // Clean then create // check for empty is already in cleanup method
        if (!mMap.isEmpty())
        {
            mMap.CleanUpWorld();
        }
        
        mMap.GenerateWorld();
        
        // This piece of code resets character position to start tile (playerbase tile) and allows the path finding to work

        mCharacter.transform.position = mMap.Start.Position; // The Start tile of the map, the position assigned from Generate method in Environment
        mCharacter.transform.rotation = Quaternion.identity;
        mCharacter.CurrentPosition = mMap.Start;

        isPlaying = true;
    }

    public void Exit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }
}
