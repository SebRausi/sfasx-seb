﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnit
{
    void TakeDamage(int amount);
    void PerformAttack();

    // Possibly add give damage
}
